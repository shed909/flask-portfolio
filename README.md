[TOC]

# Introduction

A simple portfolio website built with Flask and JavaScript.
The idea is that it integrates with the GitLab Projects API to populate cards on the "projects" page of the site.

The README.md is available for reading via a button which displays a pop-up modal box (full-screen), or the user can click the link that directs to the GitLab repository.

Since GitLab Flavoured Markdown (GFM) supports all sorts of extensions such as fenced code and highlighting, LaTeX style maths notation and TOC's, the projects can also take the form of an article.

Perhaps most importantly, it supports dark-mode theme, which is enabled by default. Theme selection is remembered with the use of a cookie.

# Project Aims

- Use Jinja templating as much as possible, minimal HTML.
- Use YAML for configuration and template content where possible, because why not?
- Eliminate the need to store project items in a database by pulling projects directly from the GitLab API (on a schedule).
- Make it fast, don't overdo the JavaScript.

# Updating Content

I built this site to utilise YAML and Jinja for a lot of the customisation and configuration, for the added ease of updates.

### Branding

The "branding" can be update via the `static/yaml/branding.yaml`:

``` yaml
name: Shiva Hamid

titles:
  - Creator
  - Destroyer

links:
  gitlab: https://gitlab.com/shivahamid
  linkedin: https://linkedin.com/in/shivahamid
  twitter: https://twitter.comshivahamid
```

### About Page

The "about" page paragraph/s can be update via the `static/yaml/about.yaml` file:

``` yaml
about:
    - This is a paragraph. It has words. The words in each list item are put into a single <p> element.

    - However, you can also add HTML tags to the words here, <h2>like this</h2>.

    - You can even add links, just check out <a target="_blank" href="https://shanehull.com/about">this</a>.

```

The template loops through these items using Jinja, and adds a `<p>` tag per YAML list item to the page.

### Projects

Projects are pulled directly from GitLab using the GitLab API, which is scheduled to update every 10 minutes using [Flask-SPScheduler](https://viniciuschiele.github.io/flask-apscheduler/).

To enable this, an API token is needed with `read_user` permissions, which can be obtained by following [this](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) guide.

The token is placed (along with the relevant GitLab URL, gtag for analytics and a debug flag) in a new `config.yaml` file:

``` yaml
gitlab-token: w4egW4yq45y3q3t4s
gitlab-url: https://gitlab.com
gtag: G-LBY82GDSRA
debug: False
```

The `static/yaml/projects.yaml` file is then populated with GitLab projects paths and project ID's, like so:

``` yaml
flask-portfolio:
  gitlab-id: 28830359

another-repository:
  gitlab-id: 28710215
```

The key in the YAML file must match the GitLab path, or it will error out and skip that project.

Projects are sorted first by latest activity, then by date created.

# Markdown Rendering for Projects

The project modals contain rendered HTML for the associated README.md file.

I'm using python-markdown with extensions to support:

- Fenced code (and code highlighting)
- Maths notation (using KaTeX to render it with JavaScript)
- Tables
- Table of contents

# TODO

- Fix TOC tags to support both `[TOC]` (currently supported) and GFM's `[[__TOC__]]`
