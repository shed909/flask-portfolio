import yaml
import markdown
import base64
from gitlab import Gitlab
from gitlab.exceptions import GitlabGetError
from collections import OrderedDict


# markdown object with mathjax and code highlighting extensions
md_extensions = ['fenced_code', 'codehilite',
                 'mdx_math', 'tables', 'toc',
                 ]

md_configs = {'mdx_math': {'use_gitlab_delimiters': True},
              }

md = markdown.Markdown(extensions=md_extensions,
                       extension_configs=md_configs
                       )


# helper function to get yaml file contents
def get_yaml(path):

    with open(path) as yaml_file:
        data = yaml.safe_load(yaml_file)

    return data


# hepler function to sort gitlab projects by last activity, then created at
def sort_by_gl_dates(projects):

    sorted_projs = OrderedDict()

    sorted_items = sorted(projects,
                          reverse=True,
                          key=lambda x: (
                                   projects[x]['last_activity_at'],
                                   projects[x]['created_at']))

    for i in sorted_items:
        for k in projects:
            if k == i:
                sorted_projs[k] = projects[k]

    return sorted_projs


# helper function to get gitlab project details and readme
# (readme converted to html)
def gl_projs(url, token):

    # get projects
    projects = get_yaml("static/yaml/projects.yaml")

    # create gitlab object
    gl = Gitlab(url=url, private_token=token)
    gl.auth()

    # loop through projects in projects.yaml and update with additional info
    for proj, info in projects.items():

        try:
            # get info for this project
            this_proj = gl.projects.get(info['gitlab-id'])

            # get readme object from gitlab
            readme_obj = this_proj.files.get(file_path='README.md', ref='main')

            # get raw markdown
            readme_md = base64.b64decode(readme_obj.content).decode('utf-8')

            # convert markdown to html
            readme_html = md.convert(readme_md)

            # get the config values we want for this project
            this_conf = {
                        'name': this_proj.name,
                        'descr': this_proj.description,
                        'url': this_proj.web_url,
                        'readme_url': this_proj.readme_url,
                        'readme_content': readme_html,
                        'created_at': this_proj.created_at,
                        'last_activity_at': this_proj.last_activity_at,
                        }

            # update projects with extra info from gitlab
            projects[this_proj.path].update(this_conf)

        except GitlabGetError as e:
            # print poject name and error on exception
            print(f"Error getting project: {proj}")
            print(e)

    # sort projects by activity, created at
    projects = sort_by_gl_dates(projects)

    return projects
