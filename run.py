from flask import Flask, render_template
from flask_apscheduler import APScheduler
import util as u

# get config from config.yaml
conf = u.get_yaml("config.yaml")

# get main portfolio details
branding = u.get_yaml("static/yaml/branding.yaml")

# get projects for first run
projects = u.gl_projs(conf['gitlab-url'], conf['gitlab-token'])

# initialize the flask app
app = Flask(__name__)

# initialize scheduler
sched = APScheduler()
sched.api_enabled = False
sched.init_app(app)
sched.start()


# update projects globally with app context every 10m
@sched.task('interval', id='do_update_projs', seconds=600,
            misfire_grace_time=300
            )
def update_projs():
    with sched.app.app_context():
        global projects
        projects = u.gl_projs(conf['gitlab-url'], conf['gitlab-token'])


# create the home page route
@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', name=branding['name'],
                           titles=branding['titles'],
                           gtag=conf['gtag'],
                           )


# about page route
@app.route("/about")
def about():
    p = u.get_yaml("static/yaml/about.yaml")
    return render_template('about.html', paragraphs=p,
                           name=branding['name'],
                           social_links=branding['links'],
                           gtag=conf['gtag'],
                           )


# projects page route
@app.route("/projects", methods=['GET'])
def portfolio():
    return render_template('projects.html', projects=projects,
                           name=branding['name'],
                           gtag=conf['gtag'],
                           )


# run everything
if __name__ == "__main__":
    app.run(debug=conf['debug'])
