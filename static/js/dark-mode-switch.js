function changeTheme(){
 var darkSwitch = document.getElementById("darkSwitch");
 if (darkSwitch.checked) {
   document.documentElement.setAttribute("data-theme","dark");
   // Add the ff. line to write to memory.
   localStorage.setItem("darkSwitch","dark");
 }
 else {
   document.documentElement.removeAttribute("data-theme")
   // Add the ff. line to write to memory.
   localStorage.setItem("darkSwitch",null);
 }
}

// If there is no local storage, set darkSwitch to "dark" as default
if (localStorage.getItem("darkSwitch") == null) {
  localStorage.setItem("darkSwitch","dark");
}
// Check local storage every time html is loaded to know which theme to use.
if (localStorage.getItem("darkSwitch")==="dark") {
 // Use dark theme.
 document.documentElement.setAttribute("data-theme","dark");
}
else {
 // Use default theme.
 document.documentElement.removeAttribute("data-theme")
}
