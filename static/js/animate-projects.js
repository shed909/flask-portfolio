
var timelineProjects = anime.timeline({loop: false})

timelineProjects
.add({
  targets: '.project-col',
  opacity: [0,1],
  easing: "linear",
  duration: 700,
  delay: (el, i) => 220 * (i+2)
})
