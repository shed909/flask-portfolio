
var timelineProfileImage = anime.timeline({loop: false})

timelineProfileImage
.add({
  targets: '.profile-image-div',
  opacity: [0,1],
  easing: "easeInQuad",
  duration: 1000,
  delay: 100
})


var timelineSocialIcons = anime.timeline({loop: false})

timelineSocialIcons
.add({
  targets: '.title-logo',
  opacity: [0,0.5],
  easing: "easeInQuad",
  duration: 400,
  delay: (el, i) => 260 * (i+1)
})

var timelineMiddleText = anime.timeline({loop: false})

timelineMiddleText
.add({
  targets: '.text-title-name',
  opacity: [0,1],
  easing: "easeInQuad",
  duration: 700,
  delay: 90,
})
.add({
  targets: '.text-content',
  opacity: [0,1],
  easing: "easeInQuad",
  duration: 400,
})
