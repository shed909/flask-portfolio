if ($(window).width() <= 796) {
  $(window).bind('scroll', function() {
       if ($(window).scrollTop() > 90) {
           $('#dark-mode-toggle').hide();
       }
       else {
           $('#dark-mode-toggle').show();
       }
  });
}
