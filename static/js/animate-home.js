
var textWrapper = document.querySelector('.home-title');
textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

var timelineTitle = anime.timeline({loop: false})

timelineTitle
.add({
  targets: '.home-title .letter',
  opacity: [0,1],
  easing: "easeInQuad",
  duration: 900,
  delay: (el, i) => 120 * (i+1)
})

var timelineSubtitle = anime.timeline({loop: false})

timelineSubtitle
.add({
  targets: '.home-sub-title',
  opacity: [0,1],
  easing: "easeInQuad",
  duration: 600,
  delay: (el, i) => 1000 * (i+2)
})
